# README Color Arena#

This README talk about Color arena, it's a 3d colored shooter arena. 

Developped by Winckell benjamin, email : winckell.benjamin.isart@gmail.com

### How to ###

* git clone this repositorie
* open him from new unity project
* build it for window setup

### Scenes descriptions ###

* mainScene : first scene you should open or build, she contain all gameobjects need to start the game, for test a level open this scene at first
* menuSubScene : she contain tutorial for explain how work the controle.
* level1SubScene : containe first waves of the game.

### Bonus descriptions ###

you could catch new bonus only when you used all monutions of your current bonus.

* Blast bonus : color red , create spiral that turn around you and shoot many bullets. quantity : 3;
* Flower bonus : color purple , create many spirals around you, quantity : 1;
* Blast bonus : color  silver , create 2 wave of bullet in front of your ship , quantity : 4;
* Bullet Time bonus : color blues , Slow the time and ennemies and prevent you from move, but allow you to target they with the sight, and when time 	come back to normal shot they with a big bullet.
