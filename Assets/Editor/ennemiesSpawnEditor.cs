﻿using UnityEngine;
using UnityEditor;
using System.Collections;
using System.Collections.Generic;

[CustomEditor(typeof(ennemiesLevelController))]
public class ennemiesSpawnEditor : Editor {	


	private ennemiesLevelController target_;
	public List<wave> level=new List<wave>();	
	private int myCurrentIndex = 0;

	private Color normal = Color.grey;
	private Color clicked = Color.green;

	private string[] textGrid = {"Spawn Down","Spawn Up","Spawn Left","Spawn Right"}; 
	private bool left;
	private bool right;
	private bool down;
	private bool up;

	private int ennemie1;
	private int ennemie2;
	private int ennemie3;
	private int ennemie4;
	private int ennemie5;
	private int ennemie6;
	private bool waveHaveToBeDestroy = true;
	private float timerBeforeNextWave = 5f;

	
	void OnEnable()
	{
		this.target_ = this.target as ennemiesLevelController;	
		if(this.target_.level.Count >0){
//			Debug.Log("ici c'est IF MOTHER FUCKER");
			level = this.target_.level;
		}
		else{
//			Debug.Log("ici c'est else");
			level = new List<wave>();
		}
		injectFromModelTocurrentEditor();
//		this.target_.n
	}

	void initFields(){
		down = false;
		up = false;
		left = false;
		right = false;

		timerBeforeNextWave = 5f;
		waveHaveToBeDestroy = false;

		ennemie1=0;
		ennemie2=0;
		ennemie3=0;
		ennemie4=0;
		ennemie5=0;
		ennemie6=0;
	}


	private void injectFromModelTocurrentEditor(){

		down = ((wave)level[myCurrentIndex]).spawns[0];
		up = ((wave)level[myCurrentIndex]).spawns[1];
		left = ((wave)level[myCurrentIndex]).spawns[2];
		right = ((wave)level[myCurrentIndex]).spawns[3];

		timerBeforeNextWave = ((wave)level[myCurrentIndex]).timeBeforeNextWave;

		waveHaveToBeDestroy = ((wave)level[myCurrentIndex]).ennemiesHaveToBeDead;

		ennemie1 = ((wave)level[myCurrentIndex]).ennemiesType1;
		ennemie2 = ((wave)level[myCurrentIndex]).ennemiesType2;
		ennemie3 = ((wave)level[myCurrentIndex]).ennemiesType3;
		ennemie4 = ((wave)level[myCurrentIndex]).ennemiesType4;
		ennemie5 = ((wave)level[myCurrentIndex]).ennemiesType5;
		ennemie6 = ((wave)level[myCurrentIndex]).ennemiesType6;		
	}

	public override void OnInspectorGUI(){
		base.OnInspectorGUI();

		EditorGUILayout.LabelField("Create new wave for your level : ");

		EditorGUILayout.Space();
		/***************************************************************************************
		 * buttons use for navigate inside level content. 
		 * 
		 ***************************************************************************************/
		EditorGUILayout.BeginHorizontal();
			
			GUI.color = Color.red;
			
			if(GUILayout.Button("decrease")){
				if(myCurrentIndex>0){
					myCurrentIndex--;
					injectFromModelTocurrentEditor();	
				}
			}
			
			GUI.color = Color.white;
		
		 	EditorGUILayout.LabelField("current wave :"+myCurrentIndex, GUILayout.Width(100));

			GUI.color = Color.green;
		
			if(GUILayout.Button("increase")){
				//Debug.Log (level.Count);
				if(myCurrentIndex==level.Count-1&& myCurrentIndex+1 == level.Count){
					myCurrentIndex++;
					initFields();
				}
				//gStyle.renderer.sharedMaterial.color = Color.red;
				if(myCurrentIndex< level.Count-1 ){
					
					myCurrentIndex++;
					injectFromModelTocurrentEditor();
				
				}
			}

		EditorGUILayout.EndHorizontal();

		EditorGUILayout.Space();

		GUI.color = Color.cyan;
		
		EditorGUILayout.LabelField("Select wave spawns:");

		EditorGUILayout.BeginVertical();

			EditorGUILayout.BeginHorizontal();
				up = EditorGUILayout.Toggle(textGrid[1],up);
				down = EditorGUILayout.Toggle(textGrid[0],down);
			EditorGUILayout.EndHorizontal();
			
			EditorGUILayout.Space();
			
			EditorGUILayout.BeginHorizontal();
				left = EditorGUILayout.Toggle(textGrid[2],left);
				right = EditorGUILayout.Toggle(textGrid[3],right);
			EditorGUILayout.EndHorizontal();

		EditorGUILayout.EndVertical();

		EditorGUILayout.Space();
		
		GUI.color = Color.yellow;

	
		//GUILayout.SelectionGrid(0,textGrid,2);
		/***************************************************************************************
		 *  buttons and fields use for define which ennemies we will instanciates and hom many 
		 *  and if we need to kill every ennemies before instanciate next wave
		 * 
		 ***************************************************************************************/
		EditorGUILayout.BeginVertical();
			
			
			EditorGUILayout.LabelField("Select ennemies quantity (q<9):");
			
			EditorGUILayout.Space();
		
			ennemie1 = EditorGUILayout.IntField("ennemie 1 quantity : ",ennemie1);
			
			EditorGUILayout.Space();
			
			ennemie2 = EditorGUILayout.IntField("ennemie 2 quantity : ",ennemie2);
			
			EditorGUILayout.Space();
			
			ennemie3 = EditorGUILayout.IntField("ennemie 3 quantity : ",ennemie3);
			
			EditorGUILayout.Space();
			
			ennemie4 = EditorGUILayout.IntField("ennemie 4 quantity : ",ennemie4);
			
			EditorGUILayout.Space();
			
			ennemie5 = EditorGUILayout.IntField("ennemie 5 quantity : ",ennemie5);
			
			EditorGUILayout.Space();
			
			ennemie6 = EditorGUILayout.IntField("ennemie 6 quantity : ",ennemie6);
			
			EditorGUILayout.Space();

			GUI.color = Color.Lerp(Color.green,Color.magenta,0.9f);
			
			// block use for define if que should kill every ennemies or let appears timer.
			EditorGUILayout.BeginHorizontal();
				
				EditorGUILayout.LabelField("Wave should be dead before the next wave:",GUILayout.Width(255));
				waveHaveToBeDestroy = EditorGUILayout.Toggle(waveHaveToBeDestroy);
				
			EditorGUILayout.EndHorizontal();

			EditorGUILayout.Space();
			
			if(!waveHaveToBeDestroy){
				EditorGUILayout.BeginHorizontal();
					EditorGUILayout.LabelField("time before next wave:");
					timerBeforeNextWave = EditorGUILayout.FloatField(timerBeforeNextWave);
				EditorGUILayout.EndHorizontal();
			}

		EditorGUILayout.EndVertical();
		//end

		/***************************************************************************************
		 *  buttons use for delete and add new wave.
		 * 
		 ***************************************************************************************/
		GUI.color = Color.gray;

		if(GUILayout.Button("Erase level Waves")){
			level = new List<wave>();
			this.target_.newListWaves();
			initFields();
			myCurrentIndex =0;
		}	
	
		EditorGUILayout.Space();
		
		
		EditorGUILayout.BeginHorizontal();

			GUI.color = Color.green;
	
		if(left||right||up||down){
			if(GUILayout.Button("Create New Wave")){
				//test if we have enable timer to the next wave and if not set to 0	
				if(waveHaveToBeDestroy){
					timerBeforeNextWave = 0f;
				}

				if(myCurrentIndex < level.Count-1|| myCurrentIndex==0 && level.Count>0)
				{
					level[myCurrentIndex] = new wave(new bool[]{down,up,left,right},timerBeforeNextWave,waveHaveToBeDestroy,ennemie1,ennemie2,ennemie3,ennemie4,ennemie5,ennemie6);
				}
				else
				{
					level.Add(new wave(new bool[]{down,up,left,right},timerBeforeNextWave,waveHaveToBeDestroy,ennemie1,ennemie2,ennemie3,ennemie4,ennemie5,ennemie6));
				}
				EditorUtility.SetDirty(this.target_);
				this.target_.saveCurrentList(level);
				myCurrentIndex++;
				
				initFields();
			}
		}
		EditorGUILayout.EndHorizontal();
			
	}


}
