﻿using UnityEngine;
using System.Collections;

public class shootForward : MonoBehaviour {
	public GameObject bulletPrefab;
	public float timerBetweenShoot;
	private float copieTimer;
	public float localTimeScale;
	// Use this for initialization
	void Start () {
		localTimeScale = 1f;
		GameManager.Instance.onPause+=enabledComponent;
		GameManager.Instance.offPause+=disabledComponent;
		GameManager.Instance.OnTimeFreeze+=this.stopTracking;
		GameManager.Instance.OnTimeUnFreeze+=this.beginTracking;
		copieTimer = timerBetweenShoot;
	}

	void beginTracking (){	
		localTimeScale = 1f;
//		_allowMovement = true;
	}

	void stopTracking (){
		localTimeScale = 0.15f;
	}

	void OnDestroy(){
		GameManager.Instance.onPause-=enabledComponent;
		GameManager.Instance.offPause-=disabledComponent;
		GameManager.Instance.OnTimeFreeze-=stopTracking;
		GameManager.Instance.OnTimeUnFreeze-=beginTracking;
	}
	void disabledComponent(){
		this.enabled=true;
	}
	void enabledComponent(){
		this.enabled=false;
	}
	// Update is called once per frame
	void Update () {
		if(timerBetweenShoot>=1){
			timerBetweenShoot-=Time.deltaTime*localTimeScale;
		}
		else{
			timerBetweenShoot = copieTimer;
			Instantiate( bulletPrefab, this.transform.position, this.transform.rotation);		//Spawn the bullet with our rotation;
			
		}
	}
}
