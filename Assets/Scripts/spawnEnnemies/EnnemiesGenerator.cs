﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class EnnemiesGenerator : MonoBehaviour {
	public GameObject[] ennemiesList;
	private int[] ennemies;
	private List<GameObject> waveInstance;
	private bool ShouldDestroyWave = false;
//	public string[] spawnSlots;

	private int ChildLength;
//	private int currentChildIndex;
	private GameObject[]spawnSlots;
	private List<int> arrayOfSlotsIndex;
	
	void Awake () {
		ChildLength = this.transform.childCount;
	}

	// Use this for initialization
	void Start () {

//		List<GameObject> localarray;
		for (int i = 0; i < ChildLength; i++) {
			this.transform.GetChild(i);

//			localarray.Add(this.transform.GetChild()
		}
	}


	void createWaveInstance(){
		arrayOfSlotsIndex =  new List<int>{0,1,2,3,4,5,6,7,8};
		for(int i = 0 ; i< ennemies.Length && i < ennemiesList.Length;i++){
			for (int o = 0 ; o < ennemies[i];o++){

				if(arrayOfSlotsIndex.Count>1){
//					Debug.Log(arrayOfSlotsIndex.Count+" before");
					int indexSpawnSlots = arrayOfSlotsIndex[Random.Range(0,arrayOfSlotsIndex.Count)];

//					Debug.Log(indexSpawnSlots +" now");

					arrayOfSlotsIndex.Remove(indexSpawnSlots);
//					Debug.Log(arrayOfSlotsIndex.Count+" after");
					
					GameObject ennemieInstance = GameObject.Instantiate(ennemiesList[i],this.transform.GetChild(indexSpawnSlots).position,this.transform.rotation) as GameObject;

					waveInstance.Add(ennemieInstance);	
				}
				else{
					Debug.Log("to many ennemies inside your wave");
				}
			}
		}
	}

	public bool testWaveDestroy(){
//		if( waveInstance!=null){
//			Debug.Log(ShouldDestroyWave);
//		}
		if( (waveInstance!=null&& waveInstance.Count == 0)||!ShouldDestroyWave){
			return true;
		}
		else
			return false;
	}

	public void initWaveInstance(bool destroyWave,int[] ennemiesArray){
		waveInstance = new List<GameObject>();
		ennemies = ennemiesArray;
		ShouldDestroyWave = destroyWave;
//		Debug.Log(this.name);
		createWaveInstance();
	} 
	// Update is called once per frame
	void Update () {

	}
}
