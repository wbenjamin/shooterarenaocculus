﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

[System.Serializable]
public class ennemiesLevelController : MonoBehaviour {
	public List<wave> level;	
	// Use this for initialization
	void Start () {
//		Debug.Log (level.Count);
		spawnController.Instance.Start();
	}
	/*Methode newListWaves
	 *  use to reset data from custom editor to this script past on game object
	 */
	public void newListWaves(){
		level = new List<wave>();
	}
	/*Methode newListWaves
	 *  use to save data from custom editor to this script past on game object
	 */
	public void saveCurrentList(List<wave> currentLevel){
		level=currentLevel;
	}
	// Update is called once per frame
	void Update () {

	}
}
