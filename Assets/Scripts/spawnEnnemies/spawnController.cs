﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class spawnController : MonoBehaviour {
	public List<wave> waveListFromGenerator;
	private static spawnController instance;
	public GameObject[] spawns;

	[HideInInspector]
	public float timer;
	[HideInInspector]
	public float lastWaveInstanceTime;

	private wave currentWaveInstance;

	public static spawnController Instance
	{
		get { return instance; }
	}
	/*Start function 
	 * call when we start scene , and we create a singleton of spawncontroller and 
	 * get data from ennemiesLevelController present inside the scene. 
	 * We store level ennemie list that contain every waves of current level.
	 * 
	 */
	public void Start()
	{
		instance = this;
		DontDestroyOnLoad(gameObject);
		// uncommment this for start game normaly
		lastWaveInstanceTime = 0f;
		if(GameObject.Find("ennemiesWaveGenerator")!=null)
		waveListFromGenerator=GameObject.Find("ennemiesWaveGenerator").GetComponent<ennemiesLevelController>().level;
//		Debug.Log(waveListFromGenerator.Count);
		//after init we lunch for the first time the wave instancier  
		lunchEnnemieWavesInstances();
		GameManager.Instance.onPause+=enabledComponent;
		GameManager.Instance.offPause+=disabledComponent;
	}

	void disabledComponent(){
		this.enabled=true;
	}
	void enabledComponent(){
		this.enabled=false;
	}
	/*lunchEnnemieWavesInstances function
	 * 
	 * instanciate at each calls , wave present inside the wavelist List.
	 * 
	 */
	private void lunchEnnemieWavesInstances(){
//		Get first wave of the waves list
//		Debug.Log(waveListFromGenerator.Count);
		currentWaveInstance = waveListFromGenerator[0]; 
//		init of new array which define each ennemies and how many are defines
		int[] ennemiesQuantity = new int[]{currentWaveInstance.ennemiesType1,
			currentWaveInstance.ennemiesType2,
			currentWaveInstance.ennemiesType3,
			currentWaveInstance.ennemiesType4,
			currentWaveInstance.ennemiesType5,
			currentWaveInstance.ennemiesType6};

//		define if this wave, had a timer before next wave instance or not
		timeWaveSeter (currentWaveInstance);
//		call of current wave inside all selected spawns
		sendWaveToSelectedSpawns (currentWaveInstance, ennemiesQuantity);
//		delete of this current Wave after his instance from waves List.
		waveListFromGenerator.RemoveAt(0);
	}

	void timeWaveSeter (wave currentWaveInstance)
	{
		if (currentWaveInstance.ennemiesHaveToBeDead) {
			lastWaveInstanceTime = -2f;
		}
		else {
			lastWaveInstanceTime = currentWaveInstance.timeBeforeNextWave;
		}
	}

	void sendWaveToSelectedSpawns (wave currentWaveInstance, int[] ennemiesQuantity)
	{
		int index =0;
		
		foreach (bool spanw_ in currentWaveInstance.spawns) {
			if (spanw_) {
				spawns [index].GetComponent<EnnemiesGenerator> ().initWaveInstance (currentWaveInstance.ennemiesHaveToBeDead, ennemiesQuantity);
			}
			index++;
		}
	}
	
	bool testCurrentEnnemiesWaveDestroy(){

//		testWaveDestroy
		if(currentWaveInstance.ennemiesHaveToBeDead){

			int indexWaveDestroy =0;
			
			foreach (GameObject spanw_ in spawns) {
				if(spanw_.GetComponent<EnnemiesGenerator> ().testWaveDestroy()){
				 	indexWaveDestroy++;
				}
			}
	//		Debug.Log(indexWaveDestroy+" voilu");
			return (indexWaveDestroy == spawns.Length)? true : false ;

		}
		else {
			return false;
		}

	}
	// Update is called once per frame
	void Update () {
//		Debug.Log (lastWaveInstanceTime+" "+testCurrentEnnemiesWaveDestroy());
		
		if(waveListFromGenerator.Count>0&&(lastWaveInstanceTime <1&& lastWaveInstanceTime >-1   || testCurrentEnnemiesWaveDestroy())){
			lunchEnnemieWavesInstances();
		}
		else if(lastWaveInstanceTime>=1){
//				Debug.Log ("ici DontDestroyOnLoad diminue lastWaveInstanceTime"+lastWaveInstanceTime.ToString());
			lastWaveInstanceTime -=Time.deltaTime;
		}
		else if(waveListFromGenerator.Count<=0){
			GameManager.Instance.fireOnwin();
		}
	}
}
