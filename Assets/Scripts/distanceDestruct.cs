﻿using UnityEngine;
using System.Collections;

public class distanceDestruct : MonoBehaviour {
	public float distanceFromPlayer;
	private GameObject player;

	// Use this for initialization
	void Start () {
		player = GameObject.FindGameObjectWithTag("Player");
	}
	// Update is called once per frame
	void Update () {
		if(player && Vector3.Distance(this.transform.position, player.transform.position)>distanceFromPlayer){
//			BroadcastMessage("Destruction");
			GameObject.Destroy(this.gameObject);
//			Debug.Log(Vector3.Distance(this.transform.position, player.transform.position));
		}
		if(player == null){
			GameObject.Destroy(this.gameObject);
		}
			
	}
}
