﻿using UnityEngine;
using System.Collections;

public class GameManager : MonoBehaviour {

	public Color targetColor;
	public GameObject cameraRef;
    public float runTime;

    private float currentTime = 0;
    public float CurrentTime
    {
        get { return currentTime; }
    }

    public delegate void TimeEventHandler();
    public delegate void TimeArgsEventHandler(float time);
    public event TimeEventHandler onRunBegin;
    public event TimeArgsEventHandler onRunEnd;

	public delegate void TimeFreeze();
	public event TimeFreeze OnTimeFreeze;
	public event TimeFreeze OnTimeUnFreeze;

	public delegate void GameOver();
	public event GameOver gameOver;

	public delegate void win();
	public event win onwin;

	public delegate void score(int scoreVal);
	public event score addScore;
	public event score removeScore;

	public delegate void pause();
	public event pause onPause;
	public event pause offPause;
	private bool isPause;

    private bool runIsRunning = false;

    private static GameManager instance;
    public static GameManager Instance
    {
        get { return instance; }
    }

    void Awake()
    {

        instance = this;
        DontDestroyOnLoad(gameObject);
        // uncommment this for start game normaly
		//Application.LoadLevel("mainScene");
		StartCoroutine("colorRandom");
		isPause = false;
    }

	void Update () 
    {
		if (runIsRunning & this.GetComponent<LevelManager>().getCurrentScene() != "menuSubScene" )
        {
//			Debug.Log(currentTime);
            currentTime += Time.deltaTime;
//            if (currentTime >= runTime )
//            {
//                if (onRunEnd != null)
//                    onRunEnd(currentTime);
//                runIsRunning = false;
//            }
        }
	}

	IEnumerator colorRandom(){
		while(0<1){
			// transition complete
			// assign the target color
			//			renderer.material.color = targetColor;
			// start a new transition
			targetColor = new Color(Random.value, Random.value, Random.value);			
	
			yield return new WaitForSeconds(1);
		}
	}
    public void StartRun()
    {
        if (runIsRunning) return;
        if (onRunBegin != null)
            onRunBegin();
        runIsRunning = true;
        currentTime = 0;
    }

	public void fireRunEnd(float i){
		if(onRunEnd!=null){
			runIsRunning = false;
			onRunEnd(i);
		}
	}
    public void Die()
    {
        runIsRunning = false;
    }

	public void fireOnTimeFreeze(){
		if(OnTimeFreeze!=null){
			OnTimeFreeze();
		}
	}
	public void fireOnTimeUnFreeze(){
		if(OnTimeUnFreeze!=null){
			OnTimeUnFreeze();
		}
		
	}

	public void fireAddScore(int scoreVal){
		if(addScore!=null){
			addScore(scoreVal);
		}
		
	}
	public void fireGameOver(){
		if(gameOver!=null){
			gameOver();
		}
	}

	public void fireOnwin(){
		if(onwin!=null){
			onwin();
		}
	}

	public void firePause(){
		if(onPause!=null && !isPause ){
			isPause=true;
			onPause();
		}
		else if(offPause!=null && isPause){
			isPause=false;
			offPause();
		
		}
	}

	public void reloadScene(){
		Application.LoadLevel("mainScene");
	}
}
