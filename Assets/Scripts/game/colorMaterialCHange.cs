﻿using UnityEngine;
using System.Collections;

public class colorMaterialCHange : MonoBehaviour {


	public float timeLeft;
	
	
	void Update()
		
	{
		
		if (timeLeft <= Time.deltaTime)
			
		{
			
			// transition complete
			
			// assign the target color

			
			renderer.material.color = GameManager.Instance.targetColor;
			timeLeft = 1.0f;

		}
		
		else
			
		{
			
			// transition in progress
			
			// calculate interpolated color
			if(renderer)
			renderer.material.color = Color.Lerp(renderer.material.color, GameManager.Instance.targetColor, Time.deltaTime / timeLeft);
			
			
			
			// update the timer
			
			timeLeft -= Time.deltaTime;
			
		}
		
	}
}
