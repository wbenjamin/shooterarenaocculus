﻿using UnityEngine;
using System.Collections;

public class timeCameraColorChange : MonoBehaviour {

	private bool timeFreeze;
	private bool timeUnFreeze;
	public Color colorTimeFreeze;
	public Color colorTimeUnfreeze;
	// Use this for initialization
	void Start () {
		timeFreeze = false;
		GameManager.Instance.OnTimeFreeze+=timeFreezeColor;
		GameManager.Instance.OnTimeUnFreeze+=timeUnFreezeColor;
	}

	public void timeFreezeColor(){
		timeFreeze=true;
	}

	public void timeUnFreezeColor(){
		timeUnFreeze=true;		
	}
	// Update is called once per frame
	void Update () {
		
		if(timeFreeze)
		{
			timeFreeze = false;
			StartCoroutine(Lerp(1f,colorTimeUnfreeze,colorTimeFreeze,true));
		}
		else if(timeUnFreeze){
			timeUnFreeze=false;
			StartCoroutine(Lerp(1f,colorTimeFreeze,colorTimeUnfreeze,false));
			
//			this.gameObject.GetComponent<OVRCameraController>().BackgroundColor = Color.Lerp(colorTimeFreeze, colorTimeUnfreeze, Time.time);
		}	
	
	}


	private IEnumerator Lerp(float time,Color a, Color b,bool testFreeze)
	{
		float elapseTime = 0f;
		while(elapseTime<time)
		{
			this.gameObject.GetComponent<OVRCameraController>().BackgroundColor = Color.Lerp( a,b, (elapseTime / time));
			
			
			// We are at the position, stop this IEnumerator
			elapseTime+=Time.deltaTime;
			
			yield return new WaitForEndOfFrame();
		}
		this.gameObject.GetComponent<OVRCameraController>().BackgroundColor = b;
//		Debug.Log(testFreeze);
	}

}
