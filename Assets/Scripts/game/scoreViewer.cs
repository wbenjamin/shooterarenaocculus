﻿using UnityEngine;
using System.Collections;

public class scoreViewer : MonoBehaviour {
	private GameObject playerRef;
	private string textMesh;
	private bool running;
	private bool gameover;
	public string end;
	private string score;
	public string YouWin;
	private bool win;
	public bool Running {
		get {
			return running;
		}
		set {
				running = value;
		}
	}

	void Awake(){
		win=false;
	}

	// Use this for initialization
	void Start () {
//		running=false;
//		initRunning();
//		textMesh = this.GetComponent<TextMesh>().text;
		gameover=false;
//		GameManager.Instance.gameOver+=;
		GameManager.Instance.onwin+=textEND;
	}

	void textEND(){
		win = true;
		GameManager.Instance.fireGameOver();
	}

	void initRunning(){
		running=true;
	}
	// Update is called once per frame
	void Update () {
		if(win){
			this.GetComponent<TextMesh>().text = YouWin+score.ToString();
		}
		else{

			if(GameObject.FindGameObjectWithTag("Player")!=null && !gameover)
			{
				score = GameObject.FindGameObjectWithTag("Player").GetComponent<playerActions>().Score.ToString();
				this.GetComponent<TextMesh>().text = "Your score : "+score;
			}
			else{
				this.GetComponent<TextMesh>().color = Color.white;
				this.GetComponent<TextMesh>().text = end.ToString()+score;
			}
		}
					
	}
}
