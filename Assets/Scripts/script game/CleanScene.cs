﻿using UnityEngine;
using System.Collections;

public class CleanScene : MonoBehaviour {

    private static string nextSceneName;

    public static void LoadBefore(string next)
    {
        nextSceneName = next;
        Application.LoadLevel("EmptyScene");
    }

    void Start()
    {
        Resources.UnloadUnusedAssets();
		Debug.Log (nextSceneName);
        Application.LoadLevel(nextSceneName);
    }
	
}
