﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class ennemieController : MonoBehaviour {
	public int life;
	public bool fire;
	public GameObject particuleDead;
	private GameObject refPlayer;
	public bool dead ;
	public bool targeterByBullet;
	public int scoreValue;
	private Color rendererBaseColor;

	public int valueRandomMin;
	public int valueRandomMax;


	public List<GameObject> bonusPrefabs;

	[SerializeField]

	public int Life {
		get {return life;}
		set { life = value;}
	}
	private bool pause;

	void enablePause(){
		pause=true;
	}

	void disablePause(){
		pause=false;
	}


	void Awake(){
		rendererBaseColor = this.renderer.material.color;

		this.valueRandomMin = -10;
		this.valueRandomMax = 10;

		this.targeterByBullet = false;
		this.dead =false;
		this.collider.enabled=true;
	}

	void backToNormalRenderer(){
		if(renderer!=null)
			this.renderer.material.color = rendererBaseColor;
	}

	// Use this for initialization
	void Start () {
		GameManager.Instance.OnTimeUnFreeze += backToNormalRenderer;
		GameManager.Instance.onPause+=enablePause;
		GameManager.Instance.offPause+=disablePause;
//		Debug.Log(this.transform.position.y);
		
		//refPlayer = this.GetComponent<movementPattern1>().target.GetComponent<playerActions>();
	}
	
	void OnTriggerEnter(Collider other){
			if(life ==0){
				this.dead = true;
			}
			else if((other.name == "bulletAutotrack"  || other.name == "bulletAutotrack(Clone)" )&& targeterByBullet == true){
				this.dead = true;
			}
			else
				this.life-=1;
	}

	public void changeColor(){
		this.targeterByBullet=true;
		this.renderer.material.color = new Color(255,0,2);
	}

	void generateBonus(int index){
		Instantiate(bonusPrefabs[index],this.transform.position,this.transform.rotation);
	}

	void Destruction(){
		int randomIndex = Random.Range(valueRandomMin,valueRandomMax);

		if(randomIndex >=0 && randomIndex <=3){
			generateBonus(randomIndex);
		}

		GameManager.Instance.OnTimeUnFreeze-=backToNormalRenderer;
		GameManager.Instance.fireAddScore(scoreValue);
		GameObject.Instantiate(particuleDead,this.transform.position,this.transform.rotation);	
		GameObject.Destroy(this.gameObject);
	}
	// Update is called once per frame
	void Update () {
		if(!pause){
			this.transform.position = new Vector3(this.transform.position.x,this.transform.position.y,this.transform.position.z);
			if(this.dead){
				this.Destruction();
			}
			//Debug.Log (Life);
		}
	
	}
}
