﻿using UnityEngine;
using System.Collections;
[System.Serializable]
public class wave{

	public int ennemiesType1;
	public int ennemiesType2;
	public int ennemiesType3;
	public int ennemiesType4;
	public int ennemiesType5;
	public int ennemiesType6;
	public bool ennemiesHaveToBeDead;
	public float timeBeforeNextWave;

	public bool[] spawns;

	public wave(bool[] mySpawns,float tempo,bool ennemiesDead,int ennemies1,int ennemies2,int ennemies3,int ennemies4,int ennemies5,int ennemies6) {
		if(mySpawns.Length>0)
			spawns = mySpawns;
		else 
			spawns = new bool[]{false,false,false,false};

		timeBeforeNextWave = tempo;
		ennemiesHaveToBeDead = ennemiesDead;

		ennemiesType1 = ennemies1 | 0;
		ennemiesType2 = ennemies2 | 0;
		ennemiesType3 = ennemies3 | 0;
		ennemiesType4 = ennemies4 | 0;
		ennemiesType5 = ennemies5 | 0;
		ennemiesType6 = ennemies6 | 0;
	}
}
