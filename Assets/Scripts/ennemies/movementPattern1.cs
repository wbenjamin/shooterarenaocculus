﻿using UnityEngine;
using System.Collections;

public class movementPattern1 : MonoBehaviour {
	public GameObject target;
	public float speed;
	public int timeBeforeTracking;
	private bool _allowMovement;
	private float capsuleHeightBak;
	private float capsuleRadiusBak;
	public float localTimeScale;
	// Use this for initialization
	void Start () {
		if(this.gameObject.name != "ennemie4(Clone)"){
			GameManager.Instance.OnTimeFreeze+=colliderRezise;
			GameManager.Instance.OnTimeUnFreeze+=colliderUnzise;
			capsuleHeightBak= this.GetComponent<CapsuleCollider>().height;
			capsuleRadiusBak = this.GetComponent<CapsuleCollider>().radius;
		}

		GameManager.Instance.OnTimeUnFreeze+=beginTracking;
		GameManager.Instance.OnTimeFreeze+=stopTracking;
		GameManager.Instance.onPause+=enabledComponent;
		GameManager.Instance.offPause+=disabledComponent;

//		Debug.Log (GameManager.Instance.cameraRef.transform.FindChild("CameraRight").gameObject.GetComponent<playerController>().getFreezeTime());
		if(!GameManager.Instance.cameraRef.GetComponentInChildren<playerController>().getFreezeTime()){
//			Debug.Log("FDEPUTERIE");
			beginTracking();
//			Debug.Break();
		}
		else{
			stopTracking();
		}

	} 

	void Awake(){

	}

	void OnDestroy(){
		if(this.gameObject.name != "ennemie4(Clone)"){
			GameManager.Instance.OnTimeFreeze-=this.colliderRezise;
			GameManager.Instance.OnTimeUnFreeze-=this.colliderUnzise;
		}
		GameManager.Instance.OnTimeFreeze-=this.stopTracking;
		GameManager.Instance.OnTimeUnFreeze-=this.beginTracking;
		GameManager.Instance.onPause-=enabledComponent;
		GameManager.Instance.offPause-=disabledComponent;
	}
	void disabledComponent(){
		this.enabled=true;
	}
	void enabledComponent(){
		this.enabled=false;
	}
	public void beginTracking (){	
		localTimeScale = 1f;
//		_allowMovement = true;
	}
	public void stopTracking (){
//		_allowMovement = false;
		localTimeScale = 0.15f;
	}

	void colliderRezise(){
		this.GetComponent<CapsuleCollider>().radius= 2;
		this.GetComponent<CapsuleCollider>().height= 4;
	}

	void colliderUnzise(){
		this.GetComponent<CapsuleCollider>().radius= capsuleRadiusBak;
		this.GetComponent<CapsuleCollider>().height= capsuleHeightBak;
	}
	// Update is called once per frame
	void Update () {
//		Debug.Log(_allowMovement);
//		if(_allowMovement){
//			this.transform.LookAt(target.transform);
//			
			transform.position += transform.forward * speed * Time.deltaTime*localTimeScale;
//			this.transform.Translate(transform.forward*(Time.deltaTime*localTimeScale)*speed);
//		}

	}
}
