﻿using UnityEngine;
using System.Collections;

public class bullet : MonoBehaviour {
	public float speed;
	public GameObject particuleDead;
	// Use this for initialization
	void Start () {

	}
	void OnTriggerEnter(Collider other){
		if(other.GetComponent<ennemieController>()&&!other.GetComponent<ennemieController>().dead)
			this.Destruction();
	}
	public void Destruction(){
		GameObject.Destroy(this.gameObject);
		GameObject.Instantiate(particuleDead,this.transform.position,this.transform.rotation);	
	}
	// Update is called once per frame
	void Update () {
		transform.position += transform.forward * speed;
	}
}
