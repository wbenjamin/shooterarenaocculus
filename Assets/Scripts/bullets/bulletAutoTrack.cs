﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class bulletAutoTrack : MonoBehaviour {
	private List<GameObject> targets;
	private	int indexTargetList;
	public int speed;
	public GameObject particuleDead;
	// Use this for initialization
	void Start () {
		indexTargetList = 0;
	}

	public void passTarget(List<GameObject> myTargets){
		targets = new List<GameObject>(myTargets);
	}
	// Update is called once per frame
	void Update () {
		if(targets!=null&& targets.Count!=0 && indexTargetList<targets.Count){
			if(targets[indexTargetList]!= null&&!targets[indexTargetList].GetComponent<ennemieController>().dead){
				transform.position= Vector3.MoveTowards(transform.position,targets[indexTargetList].transform.position,speed*Time.deltaTime);
			}
			else{
				indexTargetList+=1;
			}

		}
	}

	void OnTriggerEnter(Collider other){

		if((other.gameObject.layer == 9 && !other.GetComponent<ennemieController>().dead && other.GetComponent<ennemieController>().targeterByBullet))
		{	
//			Debug.Log(indexTargetList+" ici count de list: "+targets.Count);
			indexTargetList+=1;
			if(targets.Count<=indexTargetList)
				this.Destruction();
		}
	}

	void Destruction(){
		GameObject.Destroy(this.gameObject);
		GameObject.Instantiate(particuleDead,this.transform.position,this.transform.rotation);	
	}

}
