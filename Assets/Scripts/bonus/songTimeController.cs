﻿using UnityEngine;
using System.Collections;

public class songTimeController : MonoBehaviour {
	public float levelTimeFreeze;
	public float levelNormal;
	private float currentValue;
	private bool timeFreeze;
	// Use this for initialization
	void Start () {
		currentValue=this.audio.volume;
		levelNormal = this.audio.volume;
		GameManager.Instance.OnTimeFreeze+=lowSongLevel;
		GameManager.Instance.OnTimeUnFreeze+=hightSongLevel;
		timeFreeze = false;
	}

	void OnDestroy () {
		GameManager.Instance.OnTimeFreeze-=lowSongLevel;
		GameManager.Instance.OnTimeUnFreeze-=hightSongLevel;
	}
	void lowSongLevel(){
		timeFreeze=true;
	}

	void hightSongLevel(){
		timeFreeze=false;
	}
	
	// Update is called once per frame
	void Update () {
		if(timeFreeze && currentValue>levelTimeFreeze){
			currentValue-=Time.deltaTime/2;
			this.audio.volume = currentValue;
		}
		else if(!timeFreeze && currentValue<levelNormal){
			currentValue+=Time.deltaTime/2;
			this.audio.volume+=currentValue;
		}
	}
}
