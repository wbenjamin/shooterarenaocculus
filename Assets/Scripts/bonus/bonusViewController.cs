﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class bonusViewController : MonoBehaviour {
	public List<Material> bonusTexture;
	public List<Color>bonusColor;
	private Material basicMaterial;
	private GameObject playerRef;
	// Use this for initialization
	void Start () {
		basicMaterial = renderer.material;
		playerRef = GameObject.FindGameObjectWithTag("Player");
	}
	
	// Update is called once per frame
	void Update () {
		if(playerRef.GetComponent<playerActions>().currentShootIndex!=-1){
			this.renderer.material=bonusTexture[playerRef.GetComponent<playerActions>().currentShootIndex];
			this.renderer.material.color=bonusColor[playerRef.GetComponent<playerActions>().currentShootIndex];
		}
		else{
			this.renderer.material = basicMaterial;
		}

	}
}
