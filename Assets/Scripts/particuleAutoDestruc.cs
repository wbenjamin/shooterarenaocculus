﻿using UnityEngine;
using System.Collections;

public class particuleAutoDestruc : MonoBehaviour {

	private float particuleSyst;
	// Use this for initialization
	void Start () {
		particuleSyst = particleSystem.duration;
	}
	
	// Update is called once per frame
	void Update () {
		if(!this.particleSystem.IsAlive()){
			Object.Destroy(this.gameObject);
		}
	}
}