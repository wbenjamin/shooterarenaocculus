﻿using UnityEngine;
using System.Collections;

public class cameraEffectController : MonoBehaviour {

	void Awake(){

	}
	void OnDestroy(){
		GameManager.Instance.OnTimeUnFreeze-=normalView;
		GameManager.Instance.OnTimeFreeze-=timeFreezeView;
	}

	void normalView(){
		this.GetComponent<GlowEffect>().enabled = true;
		this.GetComponent<GrayscaleEffect>().enabled = false;
		if(this.GetComponent<AudioSource>()!=null)
		this.GetComponent<AudioSource>().Stop();
	}

	void timeFreezeView(){
		this.GetComponent<GlowEffect>().enabled = false;
		this.GetComponent<GrayscaleEffect>().enabled = true	;
		if(this.GetComponent<AudioSource>()!=null)
			this.GetComponent<AudioSource>().Play();
	}
	// Use this for initialization
	void Start () {
		GameManager.Instance.OnTimeUnFreeze+=normalView;
		GameManager.Instance.OnTimeFreeze+=timeFreezeView;
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
	