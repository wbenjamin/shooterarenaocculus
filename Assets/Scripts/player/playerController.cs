﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class playerController : MonoBehaviour {
	public GameObject lookAtObj;
	public GameObject playerRef;

	public string fireButtonX;
	public string startButton;
	public string rotationJoystickRightX;
	public string rotationJoystickRightY;
	public string buttonStopTime;

	public float secondsBeforeNextShoot;
	public float secondsBeforeNextShoot2;
	public float timerFreeze;
	private float shootTime;
	private float timerFire2;

	public bool testIsFreezeTime;
	public bool tutorielWithoutShoot;
	private bool testfire2unpressed;
	private List<GameObject> listTargetedEnemies;
	private bool pause;
	private bool startPressed;	


	private void enablePause(){
		pause=true;
	}
	private void disablePause(){
		pause=false;
	}

	void Start(){
		tutorielWithoutShoot=true;
		startPressed = true;
		GameManager.Instance.onPause+=enablePause;
		GameManager.Instance.offPause+=disablePause;
		GameManager.Instance.OnTimeFreeze+=timeFreeze;
		GameManager.Instance.OnTimeUnFreeze+=timeNotFreeze;
		GameManager.Instance.OnTimeUnFreeze+=lunchBulletAutoTrack;
		GameManager.Instance.onRunBegin+=myStart;
		timerFreeze=0f;
	}
	void OnDestroy(){
		GameManager.Instance.onPause-=enablePause;
		GameManager.Instance.onPause-=disablePause;
		GameManager.Instance.OnTimeFreeze-=timeFreeze;
		GameManager.Instance.OnTimeUnFreeze-=timeNotFreeze;
		GameManager.Instance.OnTimeUnFreeze-=lunchBulletAutoTrack;
		GameManager.Instance.onRunBegin-=myStart;
	}

	public bool getFreezeTime(){
		return this.testIsFreezeTime;
	}

	void timeFreeze(){
		this.testIsFreezeTime=true;		
	}
	void timeNotFreeze(){
		this.testIsFreezeTime=false;		
	}

	void myStart () {
		this.testfire2unpressed = true;
		this.testIsFreezeTime = false;
		this.listTargetedEnemies = new List<GameObject>();
	}

	void setFire1Timer(){
		this.shootTime = this.secondsBeforeNextShoot;
	}
	void setFire2Timer(){
		this.timerFire2 = this.secondsBeforeNextShoot2;
	}

	void MakePlayerFollowCameraCenter(){
		/*****************************************
		 * ray cast for moving player from position of ray cast on ground object tag
		 *
		 *****************************************/
		//	Physics.Raycast(cam,hit);

		RaycastHit hit; 
		
		if (Physics.Raycast(transform.position,transform.forward,out hit,Mathf.Infinity,1<<11)){
			//debug use for draw line between camera and where we look on ground
			//Debug.DrawLine(this.transform.position,hit.point);
			hit.point += new Vector3(0,3,0);
			this.lookAtObj.transform.position=hit.point;
			if(!testIsFreezeTime)
				this.playerRef.transform.position = Vector3.MoveTowards(playerRef.transform.position, hit.point,playerRef.GetComponent<playerActions>().speed*Time.deltaTime);
			else{
				this.playerRef.transform.LookAt(hit.point);
			}
		}
		if (Physics.Raycast(transform.position,transform.forward,out hit,Mathf.Infinity,~12<<9) && testIsFreezeTime){
			//debug use for draw line between camera and where we look on ground
			Debug.DrawLine(this.transform.position,hit.point);
			if( hit.collider.gameObject.GetComponent<ennemieController>()&&hit.collider.gameObject.GetComponent<ennemieController>().targeterByBullet == false){
				this.listTargetedEnemies.Add(hit.collider.gameObject);

				hit.collider.gameObject.GetComponent<ennemieController>().changeColor();
			}
			hit.point -= new Vector3(0,-1,0);
			this.lookAtObj.GetComponent<targetController>().upDatePosition(hit.point);
		}
	}
	void lunchBulletAutoTrack(){
		if(this.listTargetedEnemies.Count >0){
			this.playerRef.SendMessage("fireAutoTrackBullet",listTargetedEnemies,SendMessageOptions.DontRequireReceiver);
		}
	}
	// Update is called once per frame
	void Update () {
		if(Input.GetButton(startButton)&& !startPressed){
			GameManager.Instance.firePause();
			startPressed=true;
		}
		else if(!Input.GetButton(startButton) && startPressed){
			startPressed=false;
		}

		if(!pause){

			this.MakePlayerFollowCameraCenter();

			/*****************************************
			 * test for make fire on single bullet and with a time test on right gachette
			 *
			 *****************************************/
			//function test if fire input is pressed in this case right gachette and for cancel spam we set a timer shootTime
			if(Input.GetAxisRaw(fireButtonX)>0.3 && shootTime<1 && !testIsFreezeTime && !tutorielWithoutShoot && playerRef!= null){
				this.playerRef.SendMessage("fire",SendMessageOptions.DontRequireReceiver);
			}
			else if(shootTime>0){
				this.shootTime-=Time.deltaTime;
			}
			/*****************************************
			 *test for left gachette to make fire a lots of bullets and like before controle by a timer and bool for make user realese de left gachette
			 *
			 *****************************************/

			if(Input.GetAxisRaw(fireButtonX)<-0.1 && timerFire2<1 && testfire2unpressed && !testIsFreezeTime){
				this.playerRef.SendMessage("fire2",SendMessageOptions.DontRequireReceiver);
				this.testfire2unpressed=false;
			}
			else if (!testfire2unpressed &&Input.GetAxisRaw(fireButtonX)>-0.1){
				this.testfire2unpressed = true;
			}
			else if(timerFire2>0){
				this.timerFire2-=Time.deltaTime;
			}
			/*****************************************
			 * use for rotate player on him self from right gamepad controller
			 *
			 *****************************************/
			//use for rotate player with on stick , in this case right stick
			if((Input.GetAxisRaw(rotationJoystickRightX)> 0.3 || Input.GetAxisRaw(rotationJoystickRightY)> 0.3 || Input.GetAxisRaw(rotationJoystickRightX) < -0.3 || Input.GetAxisRaw(rotationJoystickRightY)< -0.3 )&& !testIsFreezeTime)
			{
				if(this.playerRef)
				this.playerRef.SendMessage("Rotation",SendMessageOptions.DontRequireReceiver);
			}
			/*****************************************
			 * stop Time action for targets many ennemies
			 *
			 *****************************************/
			if(timerFreeze<=0 && testIsFreezeTime){
//				this.testIsFreezeTime=false;
				timerFreeze = 5f;
				GameManager.Instance.fireOnTimeUnFreeze();
				this.listTargetedEnemies.Clear();
			}
			else
				timerFreeze-=Time.deltaTime;
		}
	}


}
