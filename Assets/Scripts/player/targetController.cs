﻿using UnityEngine;
using System.Collections;

public class targetController : MonoBehaviour {
	public Material timeFreeze ;
	private Material timeNormal ;
	public Vector3 transformTimeFreeze;
	private bool selfActive;
	private bool timeFreezeBool =false;
	public Material freezeTarget;
	private Material UnfreezeTarget;

	void Awake(){
		timeNormal = renderer.material;
		selfActive=false;
	}
	// Use this for initialization
	void Start () {
		UnfreezeTarget = renderer.material;
		GameManager.Instance.onRunBegin+=setTargetActive;
		GameManager.Instance.OnTimeFreeze+=targetTimeStop;
		GameManager.Instance.OnTimeFreeze+=switchMaterialsFreeze;
		GameManager.Instance.OnTimeUnFreeze+=targetTimeReturn;
		GameManager.Instance.OnTimeUnFreeze+=switchMaterialsUnFreeze;

	}

	void OnDestroy(){
		GameManager.Instance.onRunBegin-=setTargetActive;
		GameManager.Instance.OnTimeFreeze-=switchMaterialsFreeze;
		GameManager.Instance.OnTimeFreeze-=targetTimeStop;
		GameManager.Instance.OnTimeUnFreeze-=switchMaterialsUnFreeze;
		GameManager.Instance.OnTimeUnFreeze-=targetTimeReturn;
		
	}
	void switchMaterialsFreeze(){
		renderer.material=freezeTarget;
	}
	void switchMaterialsUnFreeze(){
		renderer.material=UnfreezeTarget;
	}
	void targetTimeStop(){
		if(this.name=="target"){
//			Debug.Log ("im going to turn"+ this.name);
			transform.localEulerAngles = transformTimeFreeze;
			timeFreezeBool=true;
		}
		else {
	
			
		}
	}

	void targetTimeReturn(){
		transform.localEulerAngles = new Vector3(0,0,0);
		timeFreezeBool=false;
		
//		Debug.Log ("im go back to normale");
		
	}

	public void upDatePosition(Vector3 pos){
		if(selfActive){
			if(timeFreezeBool){
				pos.z -=0.5f;
			}
			this.transform.position = pos;
		}

	}

	void setTargetActive(){
		selfActive=true;
	}
	// Update is called once per frame
	void Update () {
		//		if(timeFreezeBool){
//			Debug.Log ("swaaaag");
//			transform.localEulerAngles =new Vector3(transform.localEulerAngles.x,transform.localEulerAngles.y+Time.deltaTime*10,0);
//			
//		}
	}
}
