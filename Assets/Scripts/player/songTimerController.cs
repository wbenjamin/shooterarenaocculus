﻿using UnityEngine;
using System.Collections;

public class songTimerController : MonoBehaviour {
	public float startPitchValue ;
	public float endPitchValue ;
	private bool timeStatus;
	public float timerSongPitch;
	// Use this for initialization
	void Start () {
		timeStatus=false;
		this.audio.pitch = startPitchValue;
		GameManager.Instance.OnTimeFreeze+=playsong;
		GameManager.Instance.OnTimeUnFreeze+=stopsong;
	}
	void playsong(){
		audio.Play();
		timeStatus=true;
	}
	void stopsong(){
		audio.Stop();
		timeStatus=false;
		audio.pitch = startPitchValue;
		timerSongPitch=startPitchValue;
	}



	// Update is called once per frame
	void Update () {
		if(timeStatus){
			if(timerSongPitch>=1f)
				audio.pitch = timerSongPitch;
			timerSongPitch+=Time.deltaTime/2;
		}
	}
}
