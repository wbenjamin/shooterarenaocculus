﻿using UnityEngine;
using System.Collections;

[System.Serializable]
public class bonus{

	public string name;
	public int shootQuantity;
//	public IEnumerator bonusFunction;

	void initBonus( string myname, int shootQTY) {
//		bonusFunction = myFunction;
		name = myname;
		shootQuantity = shootQTY;
	}

	public bonus clone(){
		bonus ret = new bonus();
		ret.initBonus(this.name,this.shootQuantity);
		return ret;
	}
}
