﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

[System.Serializable]
public class playerActions : MonoBehaviour {
	public GameObject bulletPrefab;
	public GameObject bulletSpiral;
	public GameObject bulletFlower;
	public GameObject bulletBlast;
	public GameObject bulletAutoTrackPrefab;
	public float speed ; 
	private GameObject cameraControllerRef;
	private int score; 
	public int life;
	private int lifeBackUp;
	private float lifeTimer;
	public float timeBeforeLifeComeback;
	private float timeAfterLifeBack;
	public int currentShootIndex;
	public List<bonus> BonusAvailable;

	private bonus currentBonus;

	public string[] bonusName;

	private bool pause;
	private void enablePause(){
		pause=true;
	}
	private void disablePause(){
		pause=false;
	}

	public int Score {
		get {
			return score;
		}
	}

	void Awake () {
		GameManager.Instance.addScore+=addScore;
	}
	// Use this for initialization
	void Start () {
		GameManager.Instance.onPause+=enablePause;
		GameManager.Instance.offPause+=disablePause;
		pause = false;
		currentShootIndex=-1;
//		BonusAvailable[0].bonusFunction = Spiral;
//		BonusAvailable[1].bonusFunction = Flower;
//		BonusAvailable[2].bonusFunction = Blast;
//		BonusAvailable[3].bonusFunction = Burst;
		cameraControllerRef = GameManager.Instance.cameraRef;
		score = 0;
		lifeBackUp = life;
		lifeTimer=0;
	}


	/*****************************************
	 * methode for increase score of player.
	 *
	 *****************************************/
	public void addScore(int numb){
		score+=numb;
	}
	/*****************************************
	 * methode for fire first parternn of bullet
	 *
	 *****************************************/
	public void fire () {
		GameObject.Instantiate(bulletPrefab,this.transform.position,this.transform.rotation);
		cameraControllerRef.BroadcastMessage("setFire1Timer",SendMessageOptions.DontRequireReceiver);
	}
	// Update is called once per frame
	void Update () {
		if(!pause){
			if(lifeTimer<timeBeforeLifeComeback){
				lifeTimer+=Time.deltaTime;
			}
			else if(life<lifeBackUp && (lifeTimer > timeAfterLifeBack+0.5)){
				life+=1;
				lifeTimer+=Time.deltaTime;
				timeAfterLifeBack=Time.time;

			}
			else if(life<lifeBackUp && (lifeTimer < timeAfterLifeBack+0.5)){
				lifeTimer+=Time.deltaTime;
				
			}
		}
	}
	void fireAutoTrackBullet(List<GameObject> myTargets ){
		GameObject myBAutoTrack	 = GameObject.Instantiate(bulletAutoTrackPrefab ,this.transform.position,this.transform.rotation) as GameObject;
		myBAutoTrack.BroadcastMessage("passTarget",myTargets,SendMessageOptions.DontRequireReceiver);
	}

	void OnTriggerEnter(Collider other){
		if(other.gameObject.layer == 15 && this.currentShootIndex == -1){
			currentShootIndex=other.gameObject.GetComponent<bonusIndex>().currentIndexBonus;
//			Debug.Log (currentShootIndex);
			currentBonus=BonusAvailable[currentShootIndex].clone();
			Destroy(other.gameObject);
		
		}
		if(other.gameObject.layer == 9 && life>-1){
			if(life-1==-1){
				GameManager.Instance.fireGameOver();
			}
			else{

				life-=1;
				lifeTimer = Time.time;
				timeBeforeLifeComeback=Time.time+4;
				timeAfterLifeBack = Time.time;
				if(life==3){
					this.GetComponent<AudioSource>().Play();
				}
			}
		}
	}
	/*****************************************
	 * methode for fire many bullets 
	 *
	 *****************************************/
	public void fire2(){
//			Debug.Log(currentBonus.shootQuantity+" "+currentBonus.name);
		if(this.currentBonus.shootQuantity>0){
			this.currentBonus.shootQuantity-=1;
//			Debug.Log(currentBonus.shootQuantity+" "+currentBonus.name);
			if(this.currentBonus.name == "shootSpiral"){
				StartCoroutine(Spiral(this.transform, bulletSpiral, 25, 2, 0.1f, true));
			}
			else if(this.currentBonus.name == "shootFlower"){
				StartCoroutine(Flower(this.transform, bulletFlower, 2f, 6, 5f, 0.1f));
				
			}
			else if(this.currentBonus.name == "shootBlast"){
				StartCoroutine(Blast( this.transform, bulletBlast, 10, 2, 90f, 0.001f ));
				
			}
			else if(this.currentBonus.name == "shootBurst"){
				StartCoroutine(Burst(this.transform, bulletPrefab, 20, 5, 1.0f));
					
			}
			else if(this.currentBonus.name == "timeStop"){
				GameManager.Instance.fireOnTimeFreeze();
//				cameraControllerRef.GetComponentInChildren<playerController>().testIsFreezeTime = true;
				cameraControllerRef.GetComponentInChildren<playerController>().timerFreeze = 5f;
			}
			testCurrentBonusIndex();
			cameraControllerRef.BroadcastMessage("setFire2Timer",SendMessageOptions.DontRequireReceiver);
		}
		else{
			currentShootIndex = -1;
		}
	}

	public void Rotation(){
		
		float myInputX= Input.GetAxisRaw("rAxisX");
		float myInputY=Input.GetAxisRaw("rAxisY");
		transform.LookAt(transform.position + new Vector3(myInputX,0,myInputY));
	}


	void testCurrentBonusIndex(){
		if(this.currentBonus.shootQuantity ==0){
			currentShootIndex = -1;
		}
	} 
	

//	StartCoroutine(Spiral(shooter, bulletPrefab, 20, 2, 0.1f, true));

	IEnumerator Spiral( Transform shooter, GameObject bulletTrans, int shotNum, int volly, float shotTime, bool clockwise )
	{
		float bulletRot = shooter.eulerAngles.y;	//The y-axis rotation in degrees.
		while(volly > 0)
		{
			for( var i = 0; i < shotNum; i++)
			{
				Instantiate(bulletTrans, shooter.position, Quaternion.Euler(0, bulletRot, 0)); // Spawn the bullet with our rotation.
				if(clockwise)
				{
					bulletRot += 360.0f/shotNum;		//Increment the rotation for the next shot.
				}
				else
				{
					bulletRot -= 360.0f/shotNum;		//Increment the rotation for the next shot.
				}
				if(shotTime > 0)
				{
					yield return new WaitForSeconds( shotTime );	//Wait time between shots.
				}
			}
			volly--;	//Subtract from volly.
		}
	}

	//	StartCoroutine(Flower(shooter, bulletPrefab, 5, 6, 5, 0.1f));
	
	IEnumerator Flower( Transform shooter, GameObject bulletTrans, float flowerTime, int directions, float rotTime, float waitTime )
	{
		float bulletRot = 0.0f;
		while( flowerTime > 0 )
		{
			for( var i = 0; i < directions; i++)
			{
				Instantiate( bulletTrans, shooter.position, Quaternion.Euler( 0, bulletRot, 0));		//Spawn the bullet with our rotation;
				bulletRot += 360.0f/directions;
			} 
			bulletRot += rotTime; 
			if( bulletRot > 360)
			{
				bulletRot -= 360;
			}
			else if( bulletRot < 0 )
			{
				bulletRot += 360;
			}
			flowerTime -= waitTime;
			yield return new WaitForSeconds( waitTime );
		}
	}

	// Blast will evenly arch across the z-axis by spread in degrees divided by shotNum.
	// shooter: shots will be fired from this transform.
	// bulletTrans: the transform that will be spawned.
	// shotNum: number of bullets to fire.
	// volly: number of times the shotNum set will fire.
	// spread: the spread of the shots. All shots will be divided between the spread.
	// shotTime: Time between shots. 
	// e.g. Blast( badGuy.transform, Bullet, 10, 2, 90, 0.1 ); Will fire Bullet 10 times twice over a field of 90 degrees,
	//                                          				waiting 0.1 seconds between shots from the current position of badGuy.
	IEnumerator Blast( Transform shooter, GameObject bulletTrans, int shotNum, int volly,float spread, float shotTime )
	{
		// Check to make sure nothing breaks.
		if(shotNum <= 0)
		{
			// Make sure we have ammo.
			Debug.Log("shotNum in Blast must be greater than zero.");
			return false;
		}
		
		if( volly <= 0 )
		{
			// Volly needs to be atleast 1 so it's while loop will fire.
			Debug.Log("volly in Blast needs to be greater than zero.");
			return false;
			
		}
		
		if( shotTime < 0 )
		{
			// Negative shot time is weird.
			Debug.Log("shotTime in Blast can't be less than zero.");
			return false;
			
		}
		float bulletRot = shooter.eulerAngles.y;	//The y-axis rotation in degrees.
		if( shotNum <= 1 )
		{
			// Just fire straight.
			Instantiate(bulletTrans, shooter.position, Quaternion.Euler(0, bulletRot, 0)); 
		}
		else
		{
			while(volly > 0)
			{
				bulletRot = bulletRot - (spread/2);		//Offset the bullet rotation so it will start on one side of the z-axis and end on the other.
				for( var i = 0; i < shotNum; i++ )
				{
					Instantiate(bulletTrans, shooter.position, Quaternion.Euler(0, bulletRot, 0)); // Spawn the bullet with our rotation.
					bulletRot += spread/(shotNum-1);	//Increment the rotation for the next shot.
					if(shotTime > 0)
					{
						yield return new WaitForSeconds(shotTime);	//Wait time between shots.
					}
				}
				bulletRot = shooter.eulerAngles.y; // Reset the default angle.
				volly--;
			}
		}
	}
//	StartCoroutine(Burst(shooter, bulletPrefab, 20, 5, 1.0f));

	IEnumerator Burst( Transform shooter, GameObject bulletTans, int shotNum, int volly, float vollyTime )
	{
		float bulletRot = 0.0f;	//The y-axis rotation in degrees.
		while(volly > 0)
		{
			for( var i = 0; i < shotNum; i++)
			{
				Instantiate(bulletTans, shooter.position, Quaternion.Euler(0, bulletRot, 0));	//Spawn the bullet with our rotation.
				bulletRot += 360.0f/shotNum;		//Increment the rotation for the next shot.
			}
			bulletRot = 0.0f;
			volly--;
			yield return new WaitForSeconds( vollyTime );
		}
	}


}
