﻿using UnityEngine;
using System.Collections;

public class playerLifeController : MonoBehaviour {

	private GameObject playerRef;
	private Component grain;
	private bool gameover;
	// Use this for initialization
	private float[] correspondance = new float[] { 5f,4f,3f,2f,1f,0.5f,0f,0f,0f,0f,0f};

	private float[] correspondance1 = new float[] { 5f,4.5f,4f,3.5f,3f,2.5f,2f,1.5f,1f,0.2f,0f};
	private float[] correspondance2 = new float[] { 1f,0.9f,0.8f,0.7f,0.6f,0.5f,0.4f,0.3f,0.2f,0.1f,0f};
	void Start () {
		gameover=false;
		GameManager.Instance.gameOver+=Gameover;
		if(GameObject.FindGameObjectWithTag("Player"))
			playerRef = GameObject.FindGameObjectWithTag("Player");
	}
	void Gameover(){
		gameover=true;
	}
	// Update is called once per frame
	void Update () {
		if(!gameover){

	//		Debug.Log(playerRef.GetComponent<playerActions>().life);
			this.GetComponent<NoiseEffect>().grainIntensityMin = correspondance[ playerRef.GetComponent<playerActions>().life];
			this.GetComponent<NoiseEffect>().grainIntensityMax = correspondance[ playerRef.GetComponent<playerActions>().life];
			this.GetComponent<NoiseEffect>().scratchIntensityMin = correspondance1[ playerRef.GetComponent<playerActions>().life];
			this.GetComponent<NoiseEffect>().scratchIntensityMax = correspondance1[ playerRef.GetComponent<playerActions>().life];
			this.GetComponent<NoiseEffect>().grainSize = correspondance2[ playerRef.GetComponent<playerActions>().life];
		}
		else{
			this.GetComponent<NoiseEffect>().enabled=false;
		}
		

	}

}
