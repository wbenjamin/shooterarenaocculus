﻿using UnityEngine;
using System.Collections;

public class GameObjectDisable : MonoBehaviour {

	// Use this for initialization
	void Start () {
		GameManager.Instance.gameOver+=disableCurrentGameObject;
	
	}

	void disableCurrentGameObject(){
		if(this.gameObject!=null){
			GameManager.Instance.gameOver-=disableCurrentGameObject;
			Destroy(this.gameObject);
		}
	}
	// Update is called once per frame
	void Update () {
	
	}
}
