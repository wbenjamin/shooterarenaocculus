﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

[System.Serializable]
public class textIntro : MonoBehaviour {
	public List<string> Script;
	private int indexScript;
	private int indexScriptBefore;
	public string inputPass;
	private bool inputPassPressed;
	public GameObject bonus;
	// Use this for initialization
	void Start () {
		inputPassPressed = false;
		indexScriptBefore=-1;
		indexScript=0;
	}
	
	// Update is called once per frame
	void Update () {
		if(indexScriptBefore<indexScript && indexScript<Script.Count){
//			Debug.Log (indexScript);
			this.GetComponent<TextMesh>().text = Script[indexScript];
			if(indexScript ==2){
				GameObject.FindGameObjectWithTag("Player").particleSystem.Play();
				GameManager.Instance.cameraRef.GetComponent<timeCameraColorChange>().timeFreezeColor();
			}
			else if(indexScript==3){
				GameManager.Instance.cameraRef.GetComponent<timeCameraColorChange>().timeUnFreezeColor();
				GameManager.Instance.cameraRef.GetComponentInChildren<playerController>().enabled = true;
			}
			else if(indexScript==5){
				GameObject.FindGameObjectWithTag("Player").GetComponent<playerActions>().enabled=true;
//				GameManager.Instance.cameraRef.GetComponent<timeCameraColorChange>().timeUnFreezeColor();
				GameManager.Instance.cameraRef.GetComponentInChildren<playerController>().tutorielWithoutShoot = false;
			}
			else if(indexScript==7){
				bonus.SetActive(true);
			}
			else if(indexScript==8){
//				Debug.Log( );
				GameManager.Instance.fireRunEnd(52f);
			}
			indexScriptBefore=indexScript;
		}
//		Debug.Log(Input.GetButton(inputPass)+"  "+indexScript+"  "+Script.Count);
		if(Input.GetButton(inputPass)&& !inputPassPressed && indexScript!= Script.Count){
			inputPassPressed=true;
		}
		else if(!Input.GetButton(inputPass) && inputPassPressed){
			inputPassPressed=false;
			indexScript+=1;	
		}
	}
}
